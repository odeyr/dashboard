# Odeyr Dashboard

Le dashboard Odeyr est une interface web permettant de gérer son serveur local avec facilité.

Le dashboard est conçu en JavaScript avec le framework [Vue.js](https://vuejs.org).

## Étapes d'installation pour développement

### Installation des logiciels nécessaire

1. Installer [Node.js](https://nodejs.org) et npm
2. Installer [Git](https://git-scm.com/)
3. Cloner le repo
4. (Recommandé) Sélectionner la branche `develop`

### Configuration du dashboard

1. Dupliquer le fichier `development.env`
2. Renommer le fichier dupliqué à `.env` (Aucun nom, juste l'extension)
3. Modifier le contenu du fichier `.env` pour l'ajuster à votre système

*Note:* Il est recommandé d'installer l'[API GraphQL](https://gitlab.com/odeyr/api) en local. Si vous ne souhaitez pas installer l'API, une version de développement est disponible sur [https://develop.api.odeyr.org](https://develop.api.odeyr.org). Toutefois, à chaque déploiement de la branche `develop` de l'API, la base de données est réinitialisée.

### Installation du dashboard

1. Exécuter la commande: `npm i -g @vue/cli`
2. Exécuter la commande: `npm i`

Vous devriez maintenant être en mesure de lancer le serveur de développement du dashboard avec la commande: `npm start`
