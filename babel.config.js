module.exports = {
  presets: [[
    '@vue/app',
    { 'useBuiltIns': 'entry' }
  ]],
  plugins: [
    'graphql-tag'
  ]
}
