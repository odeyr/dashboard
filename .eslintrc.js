module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'space-before-function-paren': ['error', 'never'],
    'eqeqeq': 1,
    'indent': ['error', 2],
    'no-trailing-spaces': ['error', { 'ignoreComments': true }],
    'keyword-spacing': ['error', {
      'overrides': {
        'if': { 'after': false },
        'for': { 'after': false },
        'while': { 'after': false },
        'switch': { 'after': false },
        'catch': { 'after': false }
      }
    }
    ]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
