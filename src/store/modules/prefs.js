
const state = {
  isDarkTheme: null
}

const actions = {
  changeTheme: ({ commit }) => {
    commit('toggleDarkTheme')
  }
}
const mutations = {
  toggleDarkTheme: (state) => {
    state.isDarkTheme = !state.isDarkTheme
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
