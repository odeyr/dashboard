import apolloClient from '../../graphql'
import Cookies from 'js-cookie'
import moment from 'moment'

const state = {
  user: null,
  status: null,
  emailCooldown: null // We store this with persistance because we want to keep it at refresh
}

const getters = {
  isAuthenticated: state => !!state.user,
  isConfirmed: state => state.user && state.user.confirmationToken.confirmed,
  authStatus: state => state.status
}

const actions = {
  register: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/auth/register.gql'),
        variables: user,
        fetchPolicy: 'no-cache'
      }).then(response => {
        resolve(response.data.signup)
      }).catch(error => {
        reject(error)
      })
    })
  },
  login: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      commit('loginRequest')
      apolloClient.mutate({
        mutation: require('../../graphql/auth/login.gql'),
        variables: user,
        fetchPolicy: 'no-cache'
      }).then(response => {
        commit('loginSuccess', { user: response.data.login })
        resolve(response.data.login)
      }).catch(error => {
        commit('loginError', error)
        reject(error)
      })
    })
  },
  updateUser: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/user/updateMe.gql'),
        variables: user,
        fetchPolicy: 'no-cache'
      }).then(response => {
        commit('loginSuccess', { user: response.data.updateMe })
        resolve(response.data.updateMe)
      }).catch(error => {
        commit('loginError', error)
        reject(error)
      })
    })
  },
  resendVerificationEmail: ({ commit }, email) => {
    return new Promise((resolve, reject) => {
      let mutation
      if(email) {
        mutation = require('../../graphql/user/updateMe.gql')
      } else {
        mutation = require('../../graphql/auth/resendVerificationEmail.gql')
      }

      apolloClient.mutate({
        mutation,
        variables: {
          email
        },
        fetchPolicy: 'no-cache'
      }).then(response => {
        commit('emailSent', email)
        resolve(response.data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  sendPasswordResetToken: ({ commit }, email) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/auth/sendPasswordResetToken.gql'),
        variables: email,
        fetchPolicy: 'no-cache'
      }).then(response => {
        commit('emailSent', null)
        resolve(response)
      }).catch(error => {
        commit('emailSent', null)
        commit('loginError', error)
        reject(error)
      })
    })
  },
  resetPassword: ({ commit, state }, payload) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/auth/resetPassword.gql'),
        variables: payload,
        fetchPolicy: 'no-cache'
      }).then(response => {
        resolve(response)
      }).catch(error => {
        commit('loginError', error)
        reject(error)
      })
    })
  },
  logout: ({ commit }) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/auth/logout.gql'),
        fetchPolicy: 'no-cache'
      }).then(response => {
        commit('logout')
        commit('servers/updateSelectedServer', null, { root: true })
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  verify: ({ commit }, token) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/auth/confirm.gql'),
        variables: token,
        fetchPolicy: 'no-cache'
      }).then(response => {
        commit('confirmAccount')
        resolve(response.data.confirmAccount)
      }).catch(error => {
        reject(error)
      })
    })
  },
  emailCooldownFinished: ({ commit }) => {
    commit('emailCooldownFinished')
  }
}

const mutations = {
  loginRequest: (state) => {
    state.status = 'loading'
  },
  loginSuccess: (state, payload) => {
    state.status = 'success'
    state.user = payload.user
  },
  loginError: (state) => {
    state.status = 'error'
  },
  logout: (state) => {
    state.user = null
    state.status = null
    Cookies.remove('odeyr-refresh-token')
    Cookies.remove('odeyr-access-token')
  },
  confirmAccount: (state) => {
    state.user.confirmationToken.confirmed = true
  },
  emailSent: (state, email) => {
    if(email) state.user.email = email

    state.emailCooldown = moment(new Date()).add(1, 'minutes')
  },
  emailCooldownFinished: (state) => {
    state.emailCooldown = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
