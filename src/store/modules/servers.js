import apolloClient from '../../graphql'

const state = {
  selected: null,
  servers: null,
  added: null
}

const actions = {
  selectServer: ({ commit }, server) => {
    return new Promise((resolve, reject) => {
      commit('updateSelectedServer', server)
      resolve(server)
    })
  },
  fetchMyServers: ({ commit }) => {
    return new Promise((resolve, reject) => {
      apolloClient.query({
        query: require('../../graphql/server/myServers.gql'),
        fetchPolicy: 'network-only'
      }).then(response => {
        commit('setServers', response.data.me.servers)
        resolve(response.data.me.servers)
      }).catch(error => {
        reject(error)
      })
    })
  },
  addNewServer: ({ commit }, serverInput) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/server/addServer.gql'),
        variables: {
          name: serverInput[0],
          idGame: serverInput[1]
        }
      }).then(response => {
        commit('serverAdded', response.data.createGameServer)
        resolve(response.data.createGameServer)
      }).catch(error => {
        reject(error)
      })
    })
  },
  fetchSelectedServer: ({ commit, state }) => {
    return new Promise((resolve, reject) => {
      if(!state.selected && !state.selected.id) reject(new Error('No Server Selected'))

      apolloClient.query({
        query: require('../../graphql/server/singleServerOfMine.gql'),
        variables: {
          id: state.selected.id
        }
      }).then(response => {
        commit('updateSelectedServer', response.data.me.servers[0])
        resolve(response.data.me.servers[0])
      }).catch(error => {
        reject(error)
      })
    })
  },
  updateServer: ({ commit }, server) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/server/updateServer.gql'),
        variables: server
      }).then(response => {
        resolve(response.data.updateGameServer)
      }).catch(error => {
        reject(error)
      })
    })
  },
  deleteServer: ({ commit, state }, serverId) => {
    return new Promise((resolve, reject) => {
      apolloClient.mutate({
        mutation: require('../../graphql/server/deleteServer.gql'),
        variables: {
          id: serverId
        }
      }).then(response => {
        if(state.selected && serverId === state.selected.id) {
          commit('updateSelectedServer', null)
        }

        commit('deleteServer', serverId)
        resolve(response.data.deleteGameServer.id)
      }).catch(error => {
        reject(error)
      })
    })
  },
  fetchCurrentServerProperties: ({ state }) => {
    return new Promise((resolve, reject) => {
      if(!state.selected) {
        reject(new Error('You just select a server before'))
      }

      apolloClient.query({
        query: require('../../graphql/server/fetchServerProperties.gql'),
        variables: {
          id: state.selected.id
        },
        fetchPolicy: 'no-cache'
      }).then(response => {
        resolve(response.data.me.servers[0].properties)
      }).catch(error => {
        reject(error)
      })
    })
  },
  saveCurrentServerProperties: ({ state }, properties) => {
    return new Promise((resolve, reject) => {
      if(!state.selected) {
        reject(new Error('You just select a server before'))
      }

      apolloClient.mutate({
        mutation: require('../../graphql/server/updateGameServerProperties.gql'),
        variables: {
          id: state.selected.id,
          properties: properties
        },
        fetchPolicy: 'no-cache'
      }).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

const mutations = {
  updateSelectedServer: (state, payload) => {
    state.selected = payload
  },
  serverAdded: (state, payload) => {
    state.added = payload
    state.servers.push(payload)
  },
  setServers: (state, payload) => {
    state.servers = payload
  },
  deleteServer: (state, serverId) => {
    state.servers = state.servers.filter(server => server.id !== serverId)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
