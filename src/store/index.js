import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import auth from './modules/auth'
import servers from './modules/servers'
import prefs from './modules/prefs'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [createPersistedState({ key: 'odeyr-vuex' })],
  modules: {
    auth,
    servers,
    prefs
  }
})
