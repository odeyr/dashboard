import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

import LangTemplate from './views/LangTemplate.vue'
import { TranslationService } from './TranslationService'

import Login from './views/auth/Login'
import Register from './views/auth/Register'
import ForgotPassword from './views/auth/ForgotPassword'
import PasswordReset from './views/auth/PasswordReset'
import VerifyEmail from './views/auth/VerifyEmail'
import EmailConfirmed from './views/auth/EmailConfirmed'
import ServerSelection from './views/dashboards/ServerSelection'
import ServerControls from './views/dashboards/ServerControls'
import TelemetryInformations from './views/dashboards/TelemetryInformations'
import ServerPreferences from './views/dashboards/ServerPreferences'
import MapIntegration from './views/dashboards/MapIntegration'
import PlayersList from './views/dashboards/PlayersList'
import MyServers from './views/dashboards/MyServers'
import Profile from './views/Profile'
import EditServer from './views/EditServer'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/:lang',
      component: LangTemplate,
      beforeEnter: TranslationService.routeMiddleware,
      children: [{
        path: 'login',
        alias: '',
        name: 'Login',
        component: Login,
        meta: {
          guest: true,
          pageName: 'login'
        }
      },
      {
        path: 'register',
        name: 'Register',
        component: Register,
        meta: {
          guest: true,
          pageName: 'register'
        }
      },
      {
        path: 'forgot-password',
        name: 'ForgotPassword',
        component: ForgotPassword,
        meta: {
          guest: true,
          pageName: 'forgot_password'
        }
      },
      {
        path: 'password-reset/:token',
        name: 'PasswordReset',
        component: PasswordReset,
        meta: {
          guest: true,
          pageName: 'password_reset'
        }
      },
      {
        path: 'verify/:token?',
        name: 'VerifyEmail',
        component: VerifyEmail,
        meta: {
          layout: 'sidebar-only',
          requiresAuth: true,
          pageName: 'verify_email'
        }
      },
      {
        path: 'confirmed',
        name: 'EmailConfirmed',
        component: EmailConfirmed,
        meta: {
          layout: 'sidebar-only',
          requiresAuth: true,
          pageName: 'email_confirmed'
        }
      },
      {
        path: 'servers',
        component: LangTemplate,
        meta: {
          layout: 'sidebar-only',
          requiresAuth: true
        },
        children: [{
          path: '',
          name: 'ServerSelection',
          component: ServerSelection,
          meta: {
            layout: 'sidebar-only',
            requiresAuth: true,
            pageName: 'server_selection'
          }
        },
        {
          path: ':id',
          component: LangTemplate,
          children: [{
            path: 'controls',
            name: 'ServerControls',
            component: ServerControls,
            meta: {
              layout: 'sidebar-only',
              requiresAuth: true,
              pageName: 'server_control'
            }
          },
          {
            path: 'telemetry',
            name: 'TelemetryInformation',
            component: TelemetryInformations,
            meta: {
              layout: 'sidebar-only',
              requiresAuth: true,
              pageName: 'telemetry'
            }
          },
          {
            path: 'players-list',
            name: 'PlayersList',
            component: PlayersList,
            meta: {
              layout: 'sidebar-only',
              requiresAuth: true,
              pageName: 'player_list'
            }
          },
          {
            path: 'server-preferences',
            name: 'ServerPreferences',
            component: ServerPreferences,
            meta: {
              layout: 'sidebar-only',
              requiresAuth: true,
              pageName: 'server_preferences'
            }
          },
          {
            path: 'map-integration',
            name: 'MapIntegration',
            component: MapIntegration,
            meta: {
              layout: 'sidebar-only',
              requiresAuth: true,
              pageName: 'map_integration'
            }
          }
          ]
        }]
      },
      {
        path: 'profile',
        name: 'Profile',
        component: Profile,
        meta: {
          layout: 'sidebar-only',
          requiresAuth: true,
          pageName: 'profile'
        }
      },
      {
        path: 'edit-server/:id?',
        name: 'EditServer',
        component: EditServer,
        meta: {
          layout: 'sidebar-only',
          requiresAuth: true,
          pageName: 'edit_server'
        }
      },
      {
        path: 'my-servers',
        name: 'MyServers',
        component: MyServers,
        meta: {
          layout: 'sidebar-only',
          requiresAuth: true,
          pageName: 'my_servers'
        }
      }]
    },
    {
      // Redirect user to supported lang version.
      path: '*',
      redirect() {
        return TranslationService.getUserSupportedLang()
      }
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

/*
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if(store.getters['auth/isAuthenticated']) {
      next()
      return
    }

    let resolvedRoute = router.resolve({ name: 'Login' }, to)
    next(resolvedRoute.route)
  } else if(to.matched.some(record => record.meta.guest)) {
    if(!store.getters['auth/isAuthenticated']) {
      next()
      return
    }

    let resolvedRoute = router.resolve({ name: 'ServerSelection' }, to)
    next(resolvedRoute.route)
  } else {
    next()
  }
})
*/
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if(store.getters['auth/isAuthenticated']) {
      if((to.name === 'VerifyEmail' && !store.getters['auth/isConfirmed']) || (to.name !== 'VerifyEmail' && store.getters['auth/isConfirmed'])) {
        next()
        return
      } else if(to.name === 'VerifyEmail' && store.getters['auth/isConfirmed']) {
        let resolvedRoute = router.resolve({ name: 'EmailConfirmed' }, to)
        next(resolvedRoute.route)
        return
      }

      let resolvedRoute = router.resolve({ name: 'VerifyEmail' }, to)
      next(resolvedRoute.route)
      return
    }

    let resolvedRoute = router.resolve({ name: 'Login' }, to)
    next(resolvedRoute.route)
  } else if(to.matched.some(record => record.meta.guest)) {
    if(!store.getters['auth/isAuthenticated']) {
      next()
      return
    }

    let resolvedRoute = router.resolve({ name: 'ServerSelection' }, to)
    next(resolvedRoute.route)
  } else {
    next()
  }
})

export default router
