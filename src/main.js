import Vue from 'vue'
import App from './App.vue'
import router from './router'
import i18n from './i18n'
import VueSweetalert2 from 'vue-sweetalert2'
import moment from 'moment'
import ToggleButton from 'vue-js-toggle-button'
import VModal from 'vue-js-modal'
import VueTailwind from 'vue-tailwind'
import VueApollo from 'vue-apollo'
import Vuelidate from 'vuelidate'
import VTooltip from 'v-tooltip'
import VueClipboard from 'vue-clipboard2'
import Notifications from 'vue-notification'

import apolloClient from './graphql'
import store from './store'

import Default from './layouts/Default'
import FooterOnly from './layouts/FooterOnly'
import SidebarOnly from './layouts/SidebarOnly'
import LanguageSelector from '@/components/LanguageSelector'
import AlertsDisplay from '@/components/AlertsDisplay'
import CustomInput from '@/components/CustomInput'

Vue.use(VTooltip)

Vue.use(VueTailwind)

Vue.use(VueClipboard)

Vue.component('default-layout', Default)
Vue.component('footer-only-layout', FooterOnly)
Vue.component('sidebar-only-layout', SidebarOnly)
Vue.component('language-selector', LanguageSelector)
Vue.component('alerts-display', AlertsDisplay)
Vue.component('custom-input', CustomInput)

Vue.config.productionTip = false

Vue.prototype.moment = moment

Vue.use(ToggleButton)
Vue.use(VModal)
Vue.use(VueSweetalert2)
Vue.use(VueApollo)
Vue.use(Vuelidate)
Vue.use(Notifications)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

new Vue({
  router,
  i18n,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount('#app')
