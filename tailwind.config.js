module.exports = {
  variants: {},
  plugins: [],
  theme: {
    colors: {
      white: 'var(--white)',
      black: 'var(--black)',
      gray: 'var(--gray)',
      tooltipBackground: 'var(--tooltip-background)',
      chartLines: 'rgba(122, 122, 122, 1)',
      PingChart: {
        Background: 'rgba(76, 172, 175, 0.5)',
        Border: 'rgba(76, 172, 175, 1)'
      },
      PlayersChart: {
        Background: 'rgba(124, 175, 76, 0.5)',
        Border: 'rgba(124, 175, 76, 1)'
      },
      Bouton: {
        Next: 'rgba(100, 74, 115, 1)',
        Cancel: 'rgba(176, 176, 176, 1)'
      },
      ServerStatus: {
        stopped: 'var(--server-status-stopped)',
        running: 'var(--server-status-running)',
        loading: 'var(--server-status-loading)',
        erroring: 'var(--server-status-erroring)'
      },
      Background: 'var(--background)',
      PurpleLighter: 'var(--purple-lighter)',
      PurpleLight: 'var(--purple-light)',
      PurpleDark: 'var(--purple-dark)',
      Red: 'var(--red)',
      LightRed: 'var(--light-red)',
      GrayLighter: 'var(--gray-lighter)',
      GrayLight: 'var(--gray-light)',
      GrayDark: 'var(--gray-dark)',
      GrayDarker: 'var(--gray-darker)',
      Shadow: 'var(--shadow)',
      text: 'var(--text)',
      LineHeight: {
        loose: 30
      }
    }
  }
}
